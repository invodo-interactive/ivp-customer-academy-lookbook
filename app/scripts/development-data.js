// Default Invodo afiliate config
window.INVODO_AFF_CONFIG = {
  'test': 'true',
  'rtmpBase': 'rtmp://aoaef.invodo.com/',
  'imageBase': 'http://e.invodo.com/media/',
  'httpBase': 'http://aoael.invodo.com/media/',
  'name': 'Marsha',
  'playbuttoncolor': '',
  'aspectratio': 'WIDESCREEN',
  'defaultquality': '',
  'bufferscreenbranding': 'INVODO',
  'endofcontentbranding': 'INVODO',
  'showcliptitles': 'false',
  'ratingenabled': 'true',
  'autoplaymultipleclips': 'true',
  'playlist': 'false',
  'watermark': 'false',
  'backcolor': '000000',
  'frontcolor': 'FFFFFF',
  'secondarycolor': 'c1267d',
  'share': 'true',
  'og': 'true'
};

// Set the Invodo affiliate
window.INVODO_AFF_CONFIG.affiliate = 'academy.com';

// Create a local IVP config for testing
window.IVP_DATA = {
	"playlist": {
		"templates":[{
			"id": 1,
			"text": "<div class=\"ivp-playlist-left\"><img src=\"${image}\"></div><div class=\"ivp-playlist-body\"><h3>${title}</h3></div>"
		}],
		"items": [{
			"id": "VMH2TIGI",
			"template": 1,
			"data": {
				"title": "Dickies Video #1"
			}
		},
		{
			"id": "OLB9MSD4",
			"template": 1,
			"data": {
				"title": "Dickies Video #2"
			}
		},
		{
			"id": "6D5UQFB6",
			"template": 1,
			"data": {
				"title": "Dickies Video #3"
			}
		}]
	},
	"video": {
		"duration": 600,
		"dimensions": {
			"min-width": 400,
			"max-width": 600
		}
	},
	"poster": {
		"templates":[{
			"id": 1,
			"text": "<div class=\"ivp-btn ivp-btn--big-play\" role=\"button\" aria-live=\"polite\" tabindex=\"0\"><div><span>Play</span></div></div>"
		}],
		"items": [{
			"id": 1,
			"enabled": true,
			"template": 1,
			"data": {}
		}]
	},
	"endscreen": {
		"templates":[{
			"id": 1,
			"text": "<div class=\"ivp-btn ivp-btn--big-play\" role=\"button\" aria-live=\"polite\" tabindex=\"0\"><div><span>Play</span></div></div>"
		}],
		"items": [{
			"id": 1,
			"enabled": true,
			"template": 1,
			"data": {}
		}]
	},
	"panels": {
		"enable": true,
		"templates": [],
		"items": []
	},
	"hotspot": {
		"enable": true,
		"templates":[
			{
				"id": 1,
				"text": "<span class=\"ivp-hotspot-icon\"></span><div class=\"ivp-hotspot-title\" role=\"button\" aria-live=\"polite\"><div><span>${text}</span></div></div>"
			},
			{
				"id": 2,
				"text": "<div class=\"ivp-notification-inside\"><div class=\"ivp-notification-image\"><img src=\"${image}\"></div><div class=\"ivp-notification-text\"><h3>${product}</h3></div><div class=\"ivp-notification-learn-more\" role=\"button\"><p>Learn More</p></div></div>"
			}
		],
		"items": [
			{
				"id": 1,
				"title": "I am hotspots #1",
				"enabled": true,
				"template": 2,
				"type": "notify",
				"actions": [
					{
						"id": 1,
						"type": "card",
						"target": 3
					}
				],
				"data": {
					"product": "Up by Jawbone",
					"image": "http://invodo-ixd-production.s3.amazonaws.com/ivp/demo/mediumcard_jawbone_up.jpg"
				},
				"time": {
					"start": 1,
					"end": 5.3
				},
				"position": {
					"coords": [{
						"x": "",
						"y": ""
					}],
					"unit": "%"
				},
				"animate": {
					"duration": {
						"start": 0.25,
						"end": 0.25
					},
					"from": {
						"y": "-100%",
						"height": "0",
						"scale": "0"
					},
					"config":[{
						"scale": "1",
						"y":"0%",
						"height":"65px"
					},{
						"scale": "0",
						"opacity": 0,
						"height": 0,
					}]
				}
			},
			{
				"id": 2,
				"title": "I am hotspots #2",
				"enabled": true,
				"template": 2,
				"type": "notify",
				"actions": [{
					"id": 1,
					"type": "card",
					"target": 1
				}],
				"data": {
					"line": "Nourishing Oil Shampoo",
					"product": "Fine to Normal Hair",
					"image": "http://invodo-ixd-production.s3.amazonaws.com/ivp/demo/mediumcard_jawbone_up.jpg"
				},
				"time": {
					"start": 2,
					"end": 8.3
				},
				"position": {
					"coords": [{
						"x": "",
						"y": ""
					}],
					"unit": "%"
				},
				"animate": {
					"duration": {
						"start": 0.25,
						"end": 0.25
					},
					"from": {
						"y": "-100%",
						"height": "0",
						"scale": "0"
					},
					"config":[{
						"scale": "1",
						"y":"0%",
						"height":"65px"
					},{
						"scale": "0",
						"opacity": 0,
						"height": 0,
					}]
				}
			},
			{
				"id": 200,
				"title": "I am hotspots #2",
				"enabled": true,
				"template": 2,
				"type": "notify",
				"actions": [{
					"id": 1,
					"type": "card",
					"target": 1
				}],
				"data": {
					"line": "Nourishing Oil Shampoo",
					"product": "Fine to Normal Hair",
					"image": "http://invodo-ixd-production.s3.amazonaws.com/ivp/demo/mediumcard_jawbone_up.jpg"
				},
				"time": {
					"start": 3,
					"end": 6
				},
				"position": {
					"coords": [{
						"x": "",
						"y": ""
					}],
					"unit": "%"
				},
				"animate": {
					"duration": {
						"start": 0.25,
						"end": 0.25
					},
					"from": {
						"y": "-100%",
						"height": "0",
						"scale": "0"
					},
					"config":[{
						"scale": "1",
						"y":"0%",
						"height":"65px"
					},{
						"scale": "0",
						"opacity": 0,
						"height": 0,
					}]
				}
			},
			{
				"id": 100,
				"title": "I am hotspots #2",
				"enabled": true,
				"template": 2,
				"type": "notify",
				"actions": [{
					"id": 1,
					"type": "card",
					"target": 1
				}],
				"data": {
					"line": "Nourishing Oil Shampoo",
					"product": "Fine to Normal Hair",
					"image": "http://invodo-ixd-production.s3.amazonaws.com/ivp/demo/mediumcard_jawbone_up.jpg"
				},
				"time": {
					"start": 4,
					"end": 18.3
				},
				"position": {
					"coords": [{
						"x": "",
						"y": ""
					}],
					"unit": "%"
				},
				"animate": {
					"duration": {
						"start": 0.25,
						"end": 0.25
					},
					"from": {
						"y": "-100%",
						"height": "0",
						"scale": "0"
					},
					"config":[{
						"scale": "1",
						"y":"0%",
						"height":"65px"
					},{
						"scale": "0",
						"opacity": 0,
						"height": 0,
					}]
				}
			},
			{
				"id": 3,
				"title": "I am hotspots #3",
				"enabled": true,
				"template": 1,
				"actions": [{
					"id": 1,
					"type": "card",
					"target": 2
				}],
				"data": {
					"text": "Up by Jawbone"
				},
				"time": {
					"start": 5.6,
					"end": 8.9
				},
				"position": {
					"coords": [{
						"x": 49.9,
						"y": 70.6
					}],
					"unit": "%"
				},
				"animate": {
					"duration": {
						"start": .5,
						"end": .25
					},
					"from": {
						"y": 10,
						"opacity": 0,
						"scale": .85
					},
					"config":[
						{
							"y": 0,
							"opacity": 1,
							"scale": 1
						},
						{
							"opacity": 0,
							"y": 10
						}
					]
				}
			},
			{
				"id": 4,
				"title": "I am hotspots #4",
				"enabled": true,
				"template": 1,
				"actions": [{
					"id": 1,
					"type": "card",
					"target": 3
				}],
				"data": {
					"text": "Samsung Galaxy S4"
				},
				"time": {
					"start": 9.4,
					"end": 16.8
				},
				"position": {
					"coords": [{
						"x": 47,
						"y": 56.1
					}],
					"unit": "%"
				},
				"animate": {
					"duration": {
						"start": .5,
						"end": .25
					},
					"from": {
						"y": 10,
						"opacity": 0,
						"scale": .85
					},
					"config":[
						{
							"y": 0,
							"opacity": 1,
							"scale": 1
						},
						{
							"opacity": 0,
							"y": 10
						}
					]
				}
			},
			{
				"id": 5,
				"title": "I am hotspots #5",
				"enabled": true,
				"template": 1,
				"actions": [{
					"id": 1,
					"type": "card",
					"target": 2
				}],
				"data": {
					"text": "UP by Jawbone"
				},
				"time": {
					"start": 9.4,
					"end": 16.8
				},
				"position": {
					"coords": [{
						"x": 65.7,
						"y": 70.2
					}],
					"unit": "%"
				},
				"animate": {
					"duration": {
						"start": .5,
						"end": .25
					},
					"from": {
						"y": 10,
						"opacity": 0,
						"scale": .85
					},
					"config":[
						{
							"y": 0,
							"opacity": 1,
							"scale": 1
						},
						{
							"opacity": 0,
							"y": 10
						}
					]
				}
			},
			{
				"id": 6,
				"title": "I am hotspots #6",
				"enabled": true,
				"template": 1,
				"actions": [{
					"id": 1,
					"type": "card",
					"target": 4
				}],
				"data": {
					"text": "Logitech Mini Boombox"
				},
				"time": {
					"start": 17.3,
					"end": 19.9
				},
				"position": {
					"coords": [{
						"x": 41.3,
						"y": 74.9
					}],
					"unit": "%"
				},
				"animate": {
					"duration": {
						"start": .5,
						"end": .25
					},
					"from": {
						"y": 10,
						"opacity": 0,
						"scale": .85
					},
					"config":[
						{
							"y": 0,
							"opacity": 1,
							"scale": 1
						},
						{
							"opacity": 0,
							"y": 10
						}
					]
				}
			},
			{
				"id": 7,
				"title": "I am hotspots #7",
				"enabled": true,
				"template": 1,
				"actions": [{
					"id": 1,
					"type": "card",
					"target": 5
				}],
				"data": {
					"text": "Lenovo Yoga Tablet 8"
				},
				"time": {
					"start": 17.3,
					"end": 19.9
				},
				"position": {
					"coords": [{
						"x": 65.7,
						"y": 60.2
					}],
					"unit": "%"
				},
				"animate": {
					"duration": {
						"start": .5,
						"end": .25
					},
					"from": {
						"y": 10,
						"opacity": 0,
						"scale": .85
					},
					"config":[
						{
							"y": 0,
							"opacity": 1,
							"scale": 1
						},
						{
							"opacity": 0,
							"y": 10
						}
					]
				}
			},
			{
				"id": 8,
				"title": "I am hotspots #8",
				"enabled": true,
				"template": 1,
				"actions": [{
					"id": 1,
					"type": "card",
					"target": 4
				}],
				"data": {
					"text": "Logitech Mini Boombox"
				},
				"time": {
					"start": 20.3,
					"end": 23.7
				},
				"position": {
					"coords": [{
						"x": 35.5,
						"y": 50.2
					}],
					"unit": "%"
				},
				"animate": {
					"duration": {
						"start": .5,
						"end": .25
					},
					"from": {
						"y": 10,
						"opacity": 0,
						"scale": .85
					},
					"config":[
						{
							"y": 0,
							"opacity": 1,
							"scale": 1
						},
						{
							"opacity": 0,
							"y": 10
						}
					]
				}
			},
			{
				"id": 9,
				"title": "I am hotspots #9",
				"enabled": true,
				"template": 1,
				"actions": [{
					"id": 1,
					"type": "card",
					"target": 5
				}],
				"data": {
					"text": "Lenovo Yoga Tablet 8"
				},
				"time": {
					"start": 20.3,
					"end": 23.7
				},
				"position": {
					"coords": [{
						"x": 75.8,
						"y": 21.3
					}],
					"unit": "%"
				},
				"animate": {
					"duration": {
						"start": .5,
						"end": .25
					},
					"from": {
						"y": 10,
						"opacity": 0,
						"scale": .85
					},
					"config":[
						{
							"y": 0,
							"opacity": 1,
							"scale": 1
						},
						{
							"opacity": 0,
							"y": 10
						}
					]
				}
			},
			{
				"id": 10,
				"title": "I am hotspots #10",
				"enabled": true,
				"template": 1,
				"actions": [{
					"id": 1,
					"type": "card",
					"target": 4
				}],
				"data": {
					"text": "Logitech Mini Boombox"
				},
				"time": {
					"start": 24.2,
					"end": 27.1
				},
				"position": {
					"coords": [{
						"x": 40.9,
						"y": 76.3
					}],
					"unit": "%"
				},
				"animate": {
					"duration": {
						"start": .5,
						"end": .25
					},
					"from": {
						"y": 10,
						"opacity": 0,
						"scale": .85
					},
					"config":[
						{
							"y": 0,
							"opacity": 1,
							"scale": 1
						},
						{
							"opacity": 0,
							"y": 10
						}
					]
				}
			},
			{
				"id": 11,
				"title": "I am hotspots #11",
				"enabled": true,
				"template": 1,
				"actions": [{
					"id": 1,
					"type": "card",
					"target": 5
				}],
				"data": {
					"text": "Lenovo Yoga Tablet 8"
				},
				"time": {
					"start": 24.2,
					"end": 27.1
				},
				"position": {
					"coords": [{
						"x": 66.3,
						"y": 60.2
					}],
					"unit": "%"
				},
				"animate": {
					"duration": {
						"start": .5,
						"end": .25
					},
					"from": {
						"y": 10,
						"opacity": 0,
						"scale": .85
					},
					"config":[
						{
							"y": 0,
							"opacity": 1,
							"scale": 1
						},
						{
							"opacity": 0,
							"y": 10
						}
					]
				}
			},
			{
				"id": 12,
				"title": "I am hotspots #12",
				"enabled": true,
				"template": 1,
				"actions": [{
					"id": 1,
					"type": "card",
					"target": 6
				}],
				"data": {
					"text": "Beats by Dr. Dre Studio 2.0"
				},
				"time": {
					"start": 29.1,
					"end": 30.5
				},
				"position": {
					"coords": [{
						"x": 31.9,
						"y": 18.9
					}],
					"unit": "%"
				},
				"animate": {
					"duration": {
						"start": .5,
						"end": .25
					},
					"from": {
						"y": 10,
						"opacity": 0,
						"scale": .85
					},
					"config":[
						{
							"y": 0,
							"opacity": 1,
							"scale": 1
						},
						{
							"opacity": 0,
							"y": 10
						}
					]
				}
			},
			{
				"id": 13,
				"title": "I am hotspots #13",
				"enabled": true,
				"template": 1,
				"actions": [{
					"id": 1,
					"type": "card",
					"target": 7
				}],
				"data": {
					"text": "Sont SmartBand SWR10"
				},
				"time": {
					"start": 29.1,
					"end": 30.5
				},
				"position": {
					"coords": [{
						"x": 19.1,
						"y": 72.2
					}],
					"unit": "%"
				},
				"animate": {
					"duration": {
						"start": .5,
						"end": .25
					},
					"from": {
						"y": 10,
						"opacity": 0,
						"scale": .85
					},
					"config":[
						{
							"y": 0,
							"opacity": 1,
							"scale": 1
						},
						{
							"opacity": 0,
							"y": 10
						}
					]
				}
			},
			{
				"id": 14,
				"title": "I am hotspots #14",
				"enabled": true,
				"template": 1,
				"actions": [{
					"id": 1,
					"type": "card",
					"target": 8
				}],
				"data": {
					"text": "Google Nexus 6"
				},
				"time": {
					"start": 29.1,
					"end": 30.5
				},
				"position": {
					"coords": [{
						"x": 32.8,
						"y": 50.7
					}],
					"unit": "%"
				},
				"animate": {
					"duration": {
						"start": .5,
						"end": .25
					},
					"from": {
						"y": 10,
						"opacity": 0,
						"scale": .85
					},
					"config":[
						{
							"y": 0,
							"opacity": 1,
							"scale": 1
						},
						{
							"opacity": 0,
							"y": 10
						}
					]
				}
			},
			{
				"id": 15,
				"title": "I am hotspots #15",
				"enabled": true,
				"template": 1,
				"actions": [{
					"id": 1,
					"type": "card",
					"target": 6
				}],
				"data": {
					"text": "Beats by Dr. Dre Studio 2.0"
				},
				"time": {
					"start": 32.7,
					"end": 39
				},
				"position": {
					"coords": [{
						"x": 32.2,
						"y": 19.9
					}],
					"unit": "%"
				},
				"animate": {
					"duration": {
						"start": .5,
						"end": .25
					},
					"from": {
						"y": 10,
						"opacity": 0,
						"scale": .85
					},
					"config":[
						{
							"y": 0,
							"opacity": 1,
							"scale": 1
						},
						{
							"opacity": 0,
							"y": 10
						}
					]
				}
			},
			{
				"id": 16,
				"title": "I am hotspots #16",
				"enabled": true,
				"template": 1,
				"actions": [{
					"id": 1,
					"type": "card",
					"target": 7
				}],
				"data": {
					"text": "Sony SmartBand SWR10"
				},
				"time": {
					"start": 32.7,
					"end": 39
				},
				"position": {
					"coords": [{
						"x": 19.5,
						"y": 74.4
					}],
					"unit": "%"
				},
				"animate": {
					"duration": {
						"start": .5,
						"end": .25
					},
					"from": {
						"y": 10,
						"opacity": 0,
						"scale": .85
					},
					"config":[
						{
							"y": 0,
							"opacity": 1,
							"scale": 1
						},
						{
							"opacity": 0,
							"y": 10
						}
					]
				}
			},
			{
				"id": 17,
				"title": "I am hotspots #17",
				"enabled": true,
				"template": 1,
				"actions": [{
					"id": 1,
					"type": "card",
					"target": 8
				}],
				"data": {
					"text": "Google Nexus 6"
				},
				"time": {
					"start": 32.7,
					"end": 39
				},
				"position": {
					"coords": [{
						"x": 32.6,
						"y": 55.2
					}],
					"unit": "%"
				},
				"animate": {
					"duration": {
						"start": .5,
						"end": .25
					},
					"from": {
						"y": 10,
						"opacity": 0,
						"scale": .85
					},
					"config":[
						{
							"y": 0,
							"opacity": 1,
							"scale": 1
						},
						{
							"opacity": 0,
							"y": 10
						}
					]
				}
			}
		]
	},
	"card": {
		"enabled": true,
		"templates":[
			{
				"id": 1,
				"text": "<div class=\"ivp-card--large\"><div class=\"ivp-card-content\"><div class=\"ivp-container\"><div class=\"ivp-row\"><div class=\"ivp-col-6 ivp-col-pull-gutters ivp-card-image\"><img src=\"${image}\"></div><div class=\"ivp-col-6 ivp-card-text\"><h3>${title}</h3><div class=\"ivp-card-ctas\"><a class=\"ivp-card-cta ivp-card-cta--primary\" href=\"${ctaprimarylink}\" target=\"_blank\">${ctaprimarytext}</a></div>${text}</div></div></div></div></div>"
			},
			{
				"id": 2,
				"text": "<div class=\"ivp-card--medium\"><div class=\"ivp-card-content\"><div class=\"ivp-container\"><div class=\"ivp-row\"><div class=\"ivp-col-12 ivp-col-pull-gutters ivp-card-image\"><img src=\"${image}\"></div></div><div class=\"ivp-row\"><div class=\"ivp-col-12 ivp-card-text\"><h3>${title}</h3><div class=\"ivp-card-ctas\"><a class=\"ivp-card-cta ivp-card-cta--primary\" href=\"${ctaprimarylink}\" target=\"_blank\">${ctaprimarytext}</a></div>${text}</div></div></div></div></div>"
			},
			{
				"id": 3,
				"text": "<div class=\"ivp-card--large\"><div class=\"ivp-card-content\"><div class=\"ivp-container\"><div class=\"ivp-row\"><div class=\"ivp-col-6 ivp-col-pull-gutters ivp-card-image\"><img src=\"${image}\"></div><div class=\"ivp-col-6 ivp-card-text\"><h3>${title}</h3><p class=\"ivp-card-price\">${price}</p><div class=\"ivp-card-ctas\"><a class=\"ivp-card-cta ivp-card-cta--primary\" href=\"${ctaprimarylink}\" target=\"_blank\">${ctaprimarytext}</a><a class=\"ivp-card-cta ivp-card-cta--secondary\" href=\"${ctasecondarylink}\" target=\"_blank\">${ctasecondarytext}</a></div>${text}</div></div></div></div></div>"
			},
			{
				"id": 4,
				"text": "<div class=\"ivp-card--medium\"><div class=\"ivp-card-content\"><div class=\"ivp-container\"><div class=\"ivp-row\"><div class=\"ivp-col-12 ivp-col-pull-gutters ivp-card-image\"><img src=\"${image}\"></div></div><div class=\"ivp-row\"><div class=\"ivp-col-12 ivp-card-text\"><h3>${title}</h3><p class=\"ivp-card-price\">${price}</p><div class=\"ivp-card-ctas\"><a class=\"ivp-card-cta ivp-card-cta--primary\" href=\"${ctaprimarylink}\" target=\"_blank\">${ctaprimarytext}</a><a class=\"ivp-card-cta ivp-card-cta--secondary\" href=\"${ctasecondarylink}\" target=\"_blank\">${ctasecondarytext}</a></div>${text}</div></div></div></div></div>"
			}
		],
		"items": [{
			"id": 1,
			"title": "I am card #1",
			"template": 1,
			"data": {
				"image": "http://invodo-ixd-production.s3.amazonaws.com/ivp/demo/jawbone-large.jpg",
				"title": "UP by Jawbone",
				"price": "$99.99",
				"text": "<p>Up is a system that helps you understand how you sleep, eat and move so that you can make choices to live better.</p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum felis justo, pretium sed mauris ac, ornare ultricies sapien. Cras non luctus mi. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p><p>Vestibulum felis justo, pretium sed mauris ac, ornare ultricies sapien. Cras non luctus mi. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>",
				"ctaprimarytext": "Learn More",
				"ctaprimarylink": "#",
				"ctasecondarytext": "LEARN MORE",
				"ctasecondarylink": "#"
			},
			"position": {
				"x": '-50%',
				"y": '6%',
			},
			"animate": {
				"duration": {
					"start": .3,
					"end": .3
				},
				"from": {
					"left": "50%",
					"opacity": 0
				},
				"config":[
					{
						"y": '10%',
						"opacity": 1
					},
					{
						"opacity": 0,
						"y": "7%"
					}
				]
			}
		},
		{
			"id": 2,
			"title": "I am card #2",
			"template": 2,
			"data": {
				"image": "http://invodo-ixd-production.s3.amazonaws.com/ivp/demo/mediumcard_jawbone_up.jpg",
				"title": "UP by Jawbone",
				"price": "$99.99",
				"text": "<p>Up is a system that helps you understand how you sleep, eat and move so that you can make choices to live better. Track activity including steps, distance, calories burned, active vs. idle time.</p>",
				"ctaprimarytext": "Learn More",
				"ctaprimarylink": "https:\/\/jawbone.com\/up",
				"ctasecondarytext": "LEARN MORE",
				"ctasecondarylink": "https:\/\/jawbone.com\/up"
			},
			"position": {
				"x": '0',
				"y": '-54%'
			},
			"animate": {
				"duration": {
					"start": .3,
					"end": .3
				},
				"from": {
					"opacity": 0,
					"top": "50%",
					"right": 0
				},
				"config":[
					{
						"y": '-50%',
						"opacity": 1
					},
					{
						"opacity": 0,
						"y": "-54%"
					}
				]
			}
		},
		{
			"id": 3,
			"title": "I am card #3",
			"template": 2,
			"data": {
				"image": "http://invodo-ixd-production.s3.amazonaws.com/ivp/demo/mediumcard_s4.jpg",
				"title": "Samsung Galaxy S4",
				"price": "$99.99",
				"text": "<p>The design of the Samsung GALAXY S4 defies what’s possible. The incredibly-wide FULL HD Super AMOLED screen fits perfectly within an extraordinarily slim bezel. </p>",
				"ctaprimarytext": "Learn More",
				"ctaprimarylink": "http://www.samsung.com/global/microsite/galaxys4/",
				"ctasecondarytext": "LEARN MORE",
				"ctasecondarylink": "http://www.samsung.com/global/microsite/galaxys4/"
			},
			"position": {
				"x": '0',
				"y": '-54%'
			},
			"animate": {
				"duration": {
					"start": .3,
					"end": .3
				},
				"from": {
					"opacity": 0,
					"top": "50%",
					"right": 0
				},
				"config":[
					{
						"y": '-50%',
						"opacity": 1
					},
					{
						"opacity": 0,
						"y": "-54%"
					}
				]
			}
		},
		{
			"id": 4,
			"title": "I am card #4",
			"template": 2,
			"data": {
				"image": "http://invodo-ixd-production.s3.amazonaws.com/ivp/demo/mediumcard_logitechminiboombox.jpg",
				"title": "Logitech Mini Boombox",
				"price": "$99.99",
				"text": "<p>Now, great sound goes where you do. Share a playlist at a picnic. Watch a movie with your sweetheart under the stars. Gather around and wish a happy birthday to a faraway friend.</p>",
				"ctaprimarytext": "Learn More",
				"ctaprimarylink": "http:\/\/www.logitech.com\/en-us\/support\/mini-boombox?crid=1110",
				"ctasecondarytext": "LEARN MORE",
				"ctasecondarylink": "http:\/\/www.logitech.com\/en-us\/support\/mini-boombox?crid=1110"
			},
			"position": {
				"x": '0',
				"y": '-54%'
			},
			"animate": {
				"duration": {
					"start": .3,
					"end": .3
				},
				"from": {
					"opacity": 0,
					"top": "50%",
					"right": 0
				},
				"config":[
					{
						"y": '-50%',
						"opacity": 1
					},
					{
						"opacity": 0,
						"y": "-54%"
					}
				]
			}
		},
		{
			"id": 5,
			"title": "I am card #5",
			"template": 2,
			"data": {
				"image": "http://invodo-ixd-production.s3.amazonaws.com/ivp/demo/mediumcard_lenovo_yoga3.jpg",
				"title": "Lenovo Yoga Tablet 8",
				"price": "$99.99",
				"text": "<p>Yoga Tablet 8's pioneering design incorporates a battery cylinder and kickstand on the side of the device, shifting the center of gravity and opening up multiple usage modes: Hold, Tilt, and Stand.</p>",
				"ctaprimarytext": "Learn More",
				"ctaprimarylink": "http:\/\/shop.lenovo.com\/us\/en\/tablets\/lenovo\/yoga-tablet-series\/yoga-tablet-8\/",
				"ctasecondarytext": "LEARN MORE",
				"ctasecondarylink": "http:\/\/shop.lenovo.com\/us\/en\/tablets\/lenovo\/yoga-tablet-series\/yoga-tablet-8\/"
			},
			"position": {
				"x": '0',
				"y": '-54%'
			},
			"animate": {
				"duration": {
					"start": .3,
					"end": .3
				},
				"from": {
					"opacity": 0,
					"top": "50%",
					"right": 0
				},
				"config":[
					{
						"y": '-50%',
						"opacity": 1
					},
					{
						"opacity": 0,
						"y": "-54%"
					}
				]
			}
		},
		{
			"id": 6,
			"title": "I am card #6",
			"template": 2,
			"data": {
				"image": "http://invodo-ixd-production.s3.amazonaws.com/ivp/demo/mediumcard_beats_solo2.jpg",
				"title": "Beats by Dr. Dre Studio 2.0",
				"price": "$99.99",
				"text": "<p>The world’s most famous headphone has been completely redesigned and reimagined. The new Beats Studio® is lighter, sexier, and stronger. </p>",
				"ctaprimarytext": "Learn More",
				"ctaprimarylink": "http:\/\/www.beatsbydre.com\/headphones\/beats-beatsstudio.html",
				"ctasecondarytext": "LEARN MORE",
				"ctasecondarylink": "http:\/\/www.beatsbydre.com\/headphones\/beats-beatsstudio.html"
			},
			"position": {
				"x": '0',
				"y": '-54%'
			},
			"animate": {
				"duration": {
					"start": .3,
					"end": .3
				},
				"from": {
					"opacity": 0,
					"top": "50%",
					"right": 0
				},
				"config":[
					{
						"y": '-50%',
						"opacity": 1
					},
					{
						"opacity": 0,
						"y": "-54%"
					}
				]
			}
		},
		{
			"id": 7,
			"title": "I am card #7",
			"template": 2,
			"data": {
				"image": "http://invodo-ixd-production.s3.amazonaws.com/ivp/demo/mediumcard_sony_smart_band.jpg",
				"title": "Sony SmartBand SWR10",
				"price": "$99.99",
				"text": "<p>Log your day. Every day. Life logging accessory with notification alerts and music remote.</p>",
				"ctaprimarytext": "Learn More",
				"ctaprimarylink": "http:\/\/www.sonymobile.com\/us\/products\/smartwear\/smartband-swr10\/?utm_source=marketing-url&utm_medium=http:\/\/www.sonymobile.com\/us\/products\/smartwear\/smartband-swr10&utm_campaign=http:\/\/www.sonymobile.com\/smartbandswr10",
				"ctasecondarytext": "LEARN MORE",
				"ctasecondarylink": "http:\/\/www.sonymobile.com\/us\/products\/smartwear\/smartband-swr10\/?utm_source=marketing-url&utm_medium=http:\/\/www.sonymobile.com\/us\/products\/smartwear\/smartband-swr10&utm_campaign=http:\/\/www.sonymobile.com\/smartbandswr10"
			},
			"position": {
				"x": '0',
				"y": '-54%'
			},
			"animate": {
				"duration": {
					"start": .3,
					"end": .3
				},
				"from": {
					"opacity": 0,
					"top": "50%",
					"right": 0
				},
				"config":[
					{
						"y": '-50%',
						"opacity": 1
					},
					{
						"opacity": 0,
						"y": "-54%"
					}
				]
			}
		},
		{
			"id": 8,
			"title": "I am card #8",
			"template": 2,
			"data": {
				"image": "http://invodo-ixd-production.s3.amazonaws.com/ivp/demo/mediumcard_google_nexus6.jpg",
				"title": "Google Nexus 6",
				"price": "$99.99",
				"text": "<p>Nexus 6 helps you capture the everyday and the epic in fresh new ways. It's the slimmest and fastest Nexus phone ever made, powered by Android™ 4.4, KitKat®.</p>",
				"ctaprimarytext": "Learn More",
				"ctaprimarylink": "https:\/\/play.google.com\/store\/devices\/details\/Nexus_5_32GB_Black?id=nexus_5_black_32gb&hl=en",
				"ctasecondarytext": "LEARN MORE",
				"ctasecondarylink": "https:\/\/play.google.com\/store\/devices\/details\/Nexus_5_32GB_Black?id=nexus_5_black_32gb&hl=en"
			},
			"position": {
				"x": '0',
				"y": '-54%'
			},
			"animate": {
				"duration": {
					"start": .3,
					"end": .3
				},
				"from": {
					"opacity": 0,
					"top": "50%",
					"right": 0
				},
				"config":[
					{
						"y": '-50%',
						"opacity": 1
					},
					{
						"opacity": 0,
						"y": "-54%"
					}
				]
			}
		}
		]
	}
};
