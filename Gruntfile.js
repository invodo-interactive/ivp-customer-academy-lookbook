'use strict';

// # Globbing
// for performance reasons we're only matching one level down:
// 'test/spec/{,*/}*.js'
// If you want to recursively match all subfolders, use:
// 'test/spec/**/*.js'

module.exports = function (grunt) {

  // Time how long tasks take. Can help when optimizing build times
  require('time-grunt')(grunt);

  // Automatically load required grunt tasks
  require('jit-grunt')(grunt, {
    replace: 'grunt-text-replace'
  });

  // Configurable paths
  var config = {
    app: 'app',
    dist: 'dist',
    affiliate: 'academy.com',
    client: 'academy'
  };

  // Define the configuration for all the tasks
  grunt.initConfig({

    // Project settings
    config: config,

    // Package.json settings
    pkg: grunt.file.readJSON('package.json'),

    // Ivp settings
    ivp: grunt.file.readJSON('node_modules/invodo-ivp/package.json'),

    // AWS Credentials for deployment
    aws: grunt.file.exists('aws-creds.json') ? grunt.file.readJSON('aws-creds.json') : {},

    // Watches files for changes and runs tasks based on the changed files
    watch: {
      babel: {
        files: ['<%= config.app %>/scripts/{,*/}*.js'],
        tasks: ['babel:dist']
      },
      babelTest: {
        files: ['test/spec/{,*/}*.js'],
        tasks: ['babel:test', 'test:watch']
      },
      gruntfile: {
        files: ['Gruntfile.js']
      },
      sass: {
        files: ['<%= config.app %>/styles/{,*/}*.{scss,sass}'],
        tasks: ['sass', 'postcss']
      },
      styles: {
        files: ['<%= config.app %>/styles/{,*/}*.css'],
        tasks: ['newer:copy:styles', 'postcss']
      }
    },

    browserSync: {
      options: {
        notify: false,
        background: true,
        watchOptions: {
          ignored: ''
        }
      },
      livereload: {
        options: {
          files: [
            '<%= config.app %>/{,*/}*.html',
            '.tmp/styles/{,*/}*.css',
            '<%= config.app %>/images/{,*/}*',
            '.tmp/scripts/{,*/}*.js'
          ],
          port: 9000,
          server: {
            baseDir: ['.tmp', config.app],
            routes: {
              '/bower_components': './bower_components'
            }
          }
        }
      },
      test: {
        options: {
          port: 9001,
          open: false,
          logLevel: 'silent',
          host: 'localhost',
          server: {
            baseDir: ['./'],
            routes: {
              '/bower_components': './bower_components'
            }
          }
        }
      },
      dist: {
        options: {
          background: false,
          server: '<%= config.dist %>'
        }
      }
    },

    // Empties folders to start fresh
    clean: {
      dist: {
        files: [{
          dot: true,
          src: [
            '.tmp',
            '<%= config.dist %>/*',
            '!<%= config.dist %>/.git*'
          ]
        }]
      },
      server: '.tmp'
    },

    // Make sure code styles are up to par and there are no obvious mistakes
    eslint: {
      target: [
        'Gruntfile.js',
        '<%= config.app %>/scripts/{,*/}*.js',
        '!<%= config.app %>/scripts/vendor/*',
        'test/spec/{,*/}*.js'
      ]
    },

    // Jasmine testing framework configuration options
    jasmine: {
      all: {
        src: '.tmp/scripts/{,*/}.js',
        options: {
          vendor: [
            // Your bower_components scripts
          ],
          specs: '.tmp/spec/{,*/}*.js',
          helpers: '{test,.tmp}/helpers/{,*/}*.js',
          host: 'http://<%= browserSync.test.options.host %>:<%= browserSync.test.options.port %>'
        }
      }
    },

    // Compiles ES6 with Babel
    babel: {
      options: {
        sourceMap: true
      },
      dist: {
        files: [{
          expand: true,
          cwd: '<%= config.app %>/scripts',
          src: '{,*/}*.js',
          dest: '.tmp/scripts',
          ext: '.js'
        }]
      },
      test: {
        files: [{
          expand: true,
          cwd: 'test/spec',
          src: '{,*/}*.js',
          dest: '.tmp/spec',
          ext: '.js'
        }]
      }
    },

    // Compiles Sass to CSS and generates necessary files if requested
    sass: {
      options: {
        sourceMap: true,
        sourceMapEmbed: true,
        sourceMapContents: true,
        includePaths: ['.']
      },
      dist: {
        files: [{
          expand: true,
          cwd: '<%= config.app %>/styles',
          src: ['*.{scss,sass}'],
          dest: '.tmp/styles',
          ext: '.css'
        }]
      }
    },

    postcss: {
      options: {
        map: true,
        processors: [
          // Add vendor prefixed styles
          require('autoprefixer')({
            browsers: ['> 1%', 'last 2 versions', 'Firefox ESR']
          })
        ]
      },
      dist: {
        files: [{
          expand: true,
          cwd: '.tmp/styles/',
          src: '{,*/}*.css',
          dest: '.tmp/styles/'
        }]
      }
    },

    // Renames files for browser caching purposes
    filerev: {
      dist: {
        src: [
          '<%= config.dist %>/scripts/{,*/}*.js',
          '<%= config.dist %>/styles/{,*/}*.css',
          '<%= config.dist %>/images/{,*/}*.*',
          '<%= config.dist %>/styles/fonts/{,*/}*.*',
          '<%= config.dist %>/*.{ico,png}'
        ]
      }
    },

    // The following *-min tasks produce minified files in the dist folder
    imagemin: {
      dist: {
        files: [{
          expand: true,
          cwd: '<%= config.app %>/images',
          src: '{,*/}*.{gif,jpeg,jpg,png}',
          dest: '<%= config.dist %>/images'
        }]
      }
    },

    svgmin: {
      dist: {
        files: [{
          expand: true,
          cwd: '<%= config.app %>/images',
          src: '{,*/}*.svg',
          dest: '<%= config.dist %>/images'
        }]
      }
    },

    cssmin: {
      dist: {
        files: [{
          expand: true,
          cwd: '<%= config.dist %>',
          src: ['*.css', '!*.min.css'],
          dest: '<%= config.dist %>',
          ext: '.min.css'
        }]
      }
    },

    uglify: {
      dist: {
        files: [{
          expand: true,
          cwd: '<%= config.dist %>',
          src: ['*.js', '!*.min.js'],
          dest: '<%= config.dist %>',
          ext: '.min.js'
        }]
      }
    },

    concat: {
      dist: {}
    },

    // Copies remaining files to places other tasks can use
    copy: {
      dist: {
        files: [{
          expand: true,
          dot: true,
          cwd: '<%= config.app %>',
          dest: '<%= config.dist %>',
          src: [
            '*.{ico,png,txt}',
            'images/{,*/}*.webp',
            '{,*/}*.html',
            'styles/fonts/{,*/}*.*',
            '{,*/}*.json'
          ]
        }, {
          expand: true,
          dot: true,
          cwd: '.tmp',
          dest: '<%= config.dist %>',
          src: [
            'styles/{,*/}*.css',
            'scripts/{,*/}*.js',
            '!scripts/development-data.js'
          ]
        }, {
          expand: true,
          dot: true,
          cwd: 'bower_components/ivp/fonts',
          dest: '<%= config.dist %>/styles/fonts/ivp',
          src: ['{,*/}*.*']
        }]
      }
    },

    // Replace testing affiliate with real client affiliate
    replace: {
      affiliate: {
        src: ['<%= config.dist %>/{,*/}*.html'],
        overwrite: true,
        replacements: [{
          // replace the bower_components version of invodo.js
          from: 'bower_components/invodo.uncompressed/index.js',
          to: function() {
            return '//e.invodo.com/4.0/s/' + config.affiliate + '.js';
          }
        }, {
          // replace the ixd version of invodo.js
          from: '//e.invodo.com/4.0/s/ixd.invodo.com.js',
          to: function() {
            return '//e.invodo.com/4.0/s/' + config.affiliate + '.js';
          }
        }]
      },
      ivp: {
        src: ['<%= config.dist %>/{,*/}*.html'],
        overwrite: true,
        replacements: [{
          // replace the bower_components version of Ivp
          from: 'bower_components/ivp/ivp.js',
          to: function() {
            return '//ixd.invodo.com/ivp/' + ivp.version + '/ivp.min.js'
          }
        }, {
          // replace the node_modules version of Ivp
          from: 'node_modules/ivp/ivp.js',
          to: function() {
            return '//ixd.invodo.com/ivp/' + ivp.version + '/ivp.min.js'
          }
        }]
      }
    },

    aws_s3: {
      options: {
        accessKeyId: '<%= aws.AWSAccessKeyId %>',
        secretAccessKey: '<%= aws.AWSSecretKey %>'
      },
      prod: {
        options: {
          bucket: 'invodo-ixd-production',
          params: {
            ContentEncoding: 'gzip'
          }
        },
        files: [{
          expand: true,
          cwd: 'dist-compressed',
          src: ['**/*.*'],
          dest: 'ivp-experiences/<%= config.client %>/<%= pkg.name %>',
        }]
      }
    },

    compress: {
      dist: {
        options: {
          mode: 'gzip'
        },
        expand: true,
        cwd: '<%= config.dist %>',
        src: ['**/*.*'],
        dest: 'dist-compressed'
      }
    },

    invalidate_cloudfront: {
      options: {
        key: '<%= aws.AWSAccessKeyId %>',
        secret: '<%= aws.AWSSecretKey %>',
        distribution: 'E31ANHIEL69W6T'
      },
      production: {
        files: [{
          expand: true,
          cwd: './dist-compressed/',
          src: ['**/*'],
          filter: 'isFile',
          dest: 'ivp-experiences/<%= config.client %>/<%= pkg.name %>'
        }]
      }
    },

    // Run some tasks in parallel to speed up build process
    concurrent: {
      server: [
        'babel:dist',
        'sass'
      ],
      test: [
        'babel'
      ],
      dist: [
        'babel',
        'sass',
        'imagemin',
        'svgmin'
      ]
    }
  });


  grunt.registerTask('serve', 'start the server and preview your app', function (target) {

    if (target === 'dist') {
      return grunt.task.run(['build', 'browserSync:dist']);
    }

    grunt.task.run([
      'clean:server',
      'concurrent:server',
      'postcss',
      'browserSync:livereload',
      'watch'
    ]);
  });

  grunt.registerTask('test', function (target) {
    if (target !== 'watch') {
      grunt.task.run([
        'clean:server',
        'concurrent:test',
        'postcss'
      ]);
    }

    grunt.task.run([
      'browserSync:test',
      'jasmine'
    ]);
  });

  grunt.registerTask('build', [
    'clean:dist',
    'concurrent:dist',
    'postcss',
    'concat',
    'cssmin',
    'uglify',
    'copy:dist',
    // 'filerev'
    'replace:affiliate',
    'compress'
  ]);

  grunt.registerTask('deploy', 'Deploy the experience to s3.', function(target) {
    if(target === 'prod') {
      grunt.task.run([
        'build',
        'aws_s3:prod',
        'invalidate_cloudfront:production'
      ]);
    }
  });

  grunt.registerTask('default', [
    'newer:eslint',
    'test',
    'build'
  ]);
};
